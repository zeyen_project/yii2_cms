<?php
namespace backend\controllers;

use backend\models\LoginFailLog;
use backend\models\LoginLog;
use backend\models\User;
use Yii;
use yii\web\Controller;

/**
 * @author Zeyen
 * @date 2021-05-25
 * 2022-01-12 进行更新，进行登录的限制和登录失败记录
 */
class SiteController extends Controller
{
    public function actionLogin()
    {

        if (Yii::$app->request->isPost) {
            $ip = Yii::$app->request->getUserIP();
            $loginFailLog = new LoginFailLog();
            $fail_num =$loginFailLog->getFailNum($ip);
            if($fail_num && $fail_num>2){
                return $this->json(100, '账号已锁死，请联系管理员~');
            }
            $post = Yii::$app->request->post();
            $user = User::findOne(['name' => $post['name']]);
            $msg = '用户名或密码不正确,失败次数过多账号将会锁死,请谨慎操作~';
            if (!$user) {
                $loginFailLog->create($post['name'],$post['password']); //记录登录错误日志
                return $this->json(100, $msg);
            }
            $res = Yii::$app->security->validatePassword($post['password'], $user->password);
            if (!$res) {
                $loginFailLog->create($post['name'],$post['password']); //记录登录错误日志
                return $this->json(100, $msg);
            }
            if ($user->status == 2) {
                return $this->json(100, '该用户已被禁用，请联系管理员');
            }
            Yii::$app->user->login($user);
            $loginLog = new LoginLog();
            $loginLog->create($user->id);
            return $this->json(200, '登录成功');
        }
        if(User::findOne(Yii::$app->user->getId())){
            return $this->redirect(['/']);
        }
        return $this->render('login');
    }

    public function actionLogout(): \yii\web\Response
    {
        Yii::$app->user->logout();
        return $this->redirect(['site/login']);
    }

    /**
     * ajax 返回json数据
     * @param $status
     * @param $msg
     * @param string $data
     * @return string
     */
    public function json($status, $msg, $data = ''): string
    {
        if ($data) {
            return json_encode(['status' => $status, 'msg' => $msg, 'data' => $data]);
        } else {
            return json_encode(['status' => $status, 'msg' => $msg]);
        }
    }

}
