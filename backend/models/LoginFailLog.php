<?php

namespace backend\models;

use backend\libs\Helper;
use Predis\Client;
use Yii;

/**
 * This is the model class for table "my_login_fail_log".
 *
 * @property int $id 登陆日志
 * @property string $input_name 输入的姓名
 * @property string $input_pwd 输入的密码
 * @property string $ip ip地址
 * @property string $address 位置
 * @property string $browser 浏览器
 * @property string $os 操作系统
 * @property string $user_agent 用户代理
 * @property int $create_time 创建时间
 */
class LoginFailLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'my_login_fail_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['create_time'], 'integer'],
            [['input_name', 'os'], 'string', 'max' => 50],
            [['input_pwd', 'user_agent'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 20],
            [['address', 'browser'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'input_name' => 'Input Name',
            'input_pwd' => 'Input Pwd',
            'ip' => 'Ip',
            'address' => 'Address',
            'browser' => 'Browser',
            'os' => 'Os',
            'user_agent' => 'User Agent',
            'create_time' => 'Create Time',
        ];
    }

    public function create($input_name,$input_pwd)
    {
        $ip = Yii::$app->request->getUserIP();
        $this->input_name = $input_name;
        $this->input_pwd = $input_pwd;
        $this->ip = $ip;
        $res = Helper::getCityByIp($ip);
        $this->address = $res['province'] . ' ' . $res['city'] . ' ' . $res['district'] . ' (' . $res['isp'].')';
        $this->browser = Helper::getBrowser();
        $this->os = Helper::getOs();
        $this->user_agent = Yii::$app->request->userAgent;
        $this->create_time = time();
        $this->save(false);
        $this->setFailNum($ip,600); //记录失败次数
    }

    /**
     * 连接Redis
     * @param $database
     * @return Client
     */
    private function connectRedis($database=0): Client
    {
        $redis_config =Yii::$app->params['redis_config'];
        $redis_config['database']=$database;
        return new Client($redis_config);

    }

    /**
     * redis 获取失败的次数
     * @param $ip
     * @return string|null
     */
    public function getFailNum($ip): ?string
    {
        $client= $this->connectRedis();
        return $client->get($ip);
    }

    /**
     * redis 设置失败次数
     * @param $ip
     * @param int $out_time
     */
    public function setFailNum($ip, int $out_time=600)
    {
        $client= $this->connectRedis();
        $fail_num =$this->getFailNum($ip);
        $client->setex($ip,$out_time,((int)$fail_num+1));
    }


}


