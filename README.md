# yii2_cms

#### 介绍
基于X-admin和Yii2的开源一套纯净后台管理系统，界面简洁、清新、美观，包含后台用户，角色权限，菜单路由，登录日志，操作日志等功能，方便你在此系统的基础上开发出更加庞大的系统。

#### 软件架构
X-admin + Yii2 +LayUi+Echarts


#### 安装教程

1.  下载项目代码(通过composer create-project zeyen/yii2_cms创建 或者git clone [https://gitee.com/zeyen_project/yii2_cms.git](https://gitee.com/zeyen_project/yii2_cms.git) 进行clone下来都可以)；
2.  导入项目根目录下的yii_cms.sql文件到MySQL中，然后到代码中修改数据库配置（advanced模板数据库配置无需过多解释吧）；
3.  **由于未上传Vendor文件夹，因此需要使用者配置composer.json文件，当然本项目的composer.json文件Zeyen已配置好，只需在根目录下执行下 compose install 命令进行项目包文件的更新和安装；**
4.  配置域名指向/backend/web/目录下，并配置伪静态（可选，直接通过目录访问也是可以的）——后台
5.  配置域名指向/frontend/web/目录下，并配置伪静态（可选，直接通过目录访问也是可以的）——前端（未做开发，属于Yii2的原始页面）



#### 更改配置

​	*** 基本总的配置文件基本在common/config 目录下  Yii2 框架advanced模板配置方法无需过多解释吧**

1. ###### 更改数据库连接配置

   main.php(生产环境下)    mian-local.php(本地开发环境)

   ``

   ```php
   'components' => [
       'db' => [
           'class' => 'yii\db\Connection',
           'dsn' => 'mysql:host=localhost;dbname=yii2_admin',
           'username' => 'root',
           'password' => '123456',
           'charset' => 'utf8',
           'tablePrefix' => 'my_',
       ],
   ```

   ​	

2. ###### 更改redis配置

   params.php (生产环境下)    params-local.php(本地开发环境)，项目总的参数配置文件基本在此文件

   ``

   ```php
   'redis_config'=>[
       'host'=>'127.0.0.1',
       'port'=>'6379',
       // 'password'=>'12345', //redis服务存在密码时,填写，没设置密码时请注释
   ]
   ```

3. ###### 更改高德地图授权key

   banckend/libs/Helper.php——getCityByIp()  && getUserLocation()方法

   ``

   ```php
   $url = 'https://restapi.amap.com/v5/ip?parameters';
   $data = array(
       'output' => 'json',
       'key' => '4759a52e1a1a6196d7555402ebbafc97',  //高德地图API key，暂时可用，不确保之后Zeyen会不会删除，请自行申请更换
       'ip' => 127.0.0.1
   );
   ```

   ``

   ```php
   $key = "4759a52e1a1a6196d7555402ebbafc97"; // //高德地图API key，暂时可用，不确保之后Zeyen会不会删除，请自行申请更换
   $url = "https://restapi.amap.com/v3/ip?key=$key";
   ```

   **备注：此key是zeyen高德地图开放平台的key，暂时可是使用，但是Zeyen不确保以后是否删除，请自行申请更换**

4. ###### 配置Nginx

   此网站的Nginx配置文件中添加下面的一行代码即可，Apache请自行[百度](https://www.yiichina.com/tutorial/465)

   ```nginx
   if (!-e $request_filename) {
               rewrite  ^(.*)$  /index.php?s=/$1  last;
               break;
   }
   ```

   

#### 使用说明

1.  主菜单的图标来自于X-admin官方，如想自定义，可以修改主菜单处图标代码。
2.  操作日志默认不会记录"*/index"， "*/view"结尾的操作，即不记录展示和查看的相关操作。如需记录，可以修改公共控制器中的beforeAction方法，其中 allowUrl 数组配置不需要权限验证的路由， allowRoute 数组配置不需要记录日志的路由。
3.  整体结构为主菜单中包含子菜单，子菜单中包含子路由。
4.  节点的创建，要创建到子路由，只创建到主菜单和子菜单，会在登录后，菜单初始化时，被过滤掉。即一个主菜单下至少要有一个子菜单和一个子路由，否则不会显示出来的。
5.  **每次配置角色权限后，需要清除一下缓存，角色菜单路由是存在缓存中的。**
6.  **每次新增菜单时，请给超级管理员一个权限，这个是开发设计中的一个bug，不授权超级管路员新的菜单，将提示没有权限且不再后台菜单栏目显示（V1.0.2已修复）**

#### 账号密码

1. ​	账号密码：（当前仅一个超级管理员账号）

   - 账号：admin

   - 密码：123456	

     

#### 项目展示

1. 登录页

   ![](https://z3.ax1x.com/2021/05/25/gzNWpn.png)

2.  后台首页

    [![7nyAIS.png](https://s4.ax1x.com/2022/01/12/7nyAIS.png)](https://imgtu.com/i/7nyAIS)

3.  菜单展示页

    ![](https://z3.ax1x.com/2021/05/25/gzUenf.png)





#### 第三方库

​	一、WorkerMan + GatewayClient 实现的websocket 功能:

​	使用说明 ：启动命令已封装到控制台任务重，可直接使用php yii 命令启动服务，Events处理类封装在						common\workerman中。

```bash
php yii workerman/start start - d  DEBUG 模式启动
php yii workerman/start start   生产模式启动
php yii workerman/start stop    停止
php yii workerman/start restart 重启
php yii workerman/start reload  重载
php yii workerman/start status  查看状态
php yii workerman/start connections  获取连接
```

​	详细文档：[Workerman官网](https://www.workerman.net/)  、 [GatewayWorker 手册](https://www.workerman.net/doc/gateway-worker/#GatewayWorker)   、 [与框架结合文档](https://www.workerman.net/doc/gateway-worker/work-with-other-frameworks.html#与ThinkPHP等框架结合)

​	

#### 作者信息

1.  Zeyen  邮箱 zeyen0186@aliyun.com
2.  Zeyen 官方博客（已弃用） [www.zeyen.cn ](https://www.zeyen.cn)
3.  你可以 [https://gitee.com/athenazetan](https://gitee.com/athenazetan) 这个地址来了解 Zeyen的其他优秀开源项目

#### 特别鸣谢

特别感谢Layui, X-admin, Yii2。
