<?php
namespace common\components;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\log\FileTarget;
use yii\log\Logger;

/**
 * 日志写入 组件
 * @author Zeyen
 * @date 2022-04-13
 */
class WriteLog extends Component
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
    }

    /**
     * 写日志  -通用
     * @author Zeyen
     * @date 2022-04-13
     * @param $filename
     * @param $message
     * @return void
     */
    public function write($filename,$message)
    {
        $logPath = Yii::$app->getRuntimePath() . '/logs/' . date("Ymd") . "/";
        try {
            FileHelper::createDirectory($logPath);
            $file = new FileTarget();
            $file->logFile = $logPath . $filename;
            $file->messages[] = [$message, Logger::LEVEL_INFO, '', time()];
            $file->export();
        } catch (Exception $e) {
            return ;
        }
    }
}
