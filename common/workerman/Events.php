<?php
namespace common\workerman;
use GatewayWorker\Lib\Gateway;

class Events
{
    // businessWorker进程启动事件
    public static function onWorkerStart($businessWorker)
    {

    }
    //连接事件
    public static function onConnect($client_id)
    {
        // 向当前client_id发送数据

    }

    //进程连接事件
    public static function onWebSocketConnect($client_id, $data)
    {

    }

    //客户端发送消息事件
    public static function onMessage($client_id, $message)
    {

    }
    // 连接断开事件
    public static function onClose($client_id)
    {

    }

}
