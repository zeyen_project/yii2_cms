<?php
namespace common\services;


use yii\helpers\Json;
use yii\web\ErrorHandler;
use Yii;
class ErrorHandle extends ErrorHandler
{
    // 重写ErrorHandler 类的renderException 方法，实现重构错误处理
    public function renderException($exception)
    {
        $errMsg = "文件位置：{$exception->getFile()} 所在行：{$exception->getLine()}\n". "错误：".$exception->getMessage();
        $info['code']=Yii::$app->response->statusCode;
        $info['msg']=$exception->getMessage();
        $info['errMsg']=$errMsg;
        Yii::$app->WriteLog->write('sys_exception.log',$errMsg);
        echo  Json::encode($info);exit;
    }

}
