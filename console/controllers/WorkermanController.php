<?php
namespace console\controllers;
use common\workerman\Events;
use GatewayWorker\BusinessWorker;
use GatewayWorker\Gateway;
use GatewayWorker\Register;
use Workerman\Worker;
use yii\console\Controller;

class WorkermanController extends Controller
{
    public function actionStart()
    {
        $this->startGateWay();
        $this->startBusinessWorker();
        $this->startRegister();
        Worker::runAll();
    }

    private function startBusinessWorker()
    {
        $worker                  = new BusinessWorker();
        //work名称
        $worker->name            = 'ZEYENBusinessWorker';
        //businessWork进程数
        $worker->count           = 2;
        //服务注册地址
        $worker->registerAddress = '127.0.0.1:9528';
        //设置\App\Workerman\Events类来处理业务
        $worker->eventHandler    = Events::class;
    }
    private function startGateWay()
    {
        //gateway进程
        $gateway = new Gateway("websocket://0.0.0.0:9527");
        //gateway名称 status方便查看
        $gateway->name                 = 'ZEYENSGateway';
        //gateway进程
        $gateway->count                = 2;
        //本机ip
        $gateway->lanIp                = '127.0.0.1';
        //内部通讯起始端口，如果$gateway->count = 4 起始端口为2300
        //则一般会使用 2300，2301 2个端口作为内部通讯端口
        $gateway->startPort            = 2300;
        //心跳间隔
        $gateway->pingInterval         = 30;
        //客户端连续$pingNotResponseLimit次$pingInterval时间内不发送任何数据则断开链接，并触发onClose。
        //我们这里使用的是服务端主动发送心跳所以设置为0
        $gateway->pingNotResponseLimit = 0;
        //心跳数据
        $gateway->pingData             = '{"type":"@heart@"}';
        //服务注册地址
        $gateway->registerAddress      = '127.0.0.1:9528';
    }
    private function startRegister()
    {
        new Register('text://0.0.0.0:9528');
    }

}
