/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 127.0.0.1:3306
 Source Schema         : yii2_admin

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 12/01/2022 15:02:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for my_login_fail_log
-- ----------------------------
DROP TABLE IF EXISTS `my_login_fail_log`;
CREATE TABLE `my_login_fail_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '登陆日志',
  `input_name` varchar(50) DEFAULT NULL COMMENT '输入的姓名',
  `input_pwd` varchar(255) DEFAULT NULL COMMENT '输入的密码',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip地址',
  `address` varchar(100) DEFAULT NULL COMMENT '位置',
  `browser` varchar(100) DEFAULT NULL COMMENT '浏览器',
  `os` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `user_agent` varchar(255) DEFAULT NULL COMMENT '用户代理',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_login_fail_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for my_login_log
-- ----------------------------
DROP TABLE IF EXISTS `my_login_log`;
CREATE TABLE `my_login_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '登陆日志',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `ip` varchar(20) DEFAULT NULL COMMENT 'ip地址',
  `address` varchar(100) DEFAULT NULL COMMENT '位置',
  `browser` varchar(100) DEFAULT NULL COMMENT '浏览器',
  `os` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `user_agent` varchar(255) DEFAULT NULL COMMENT '用户代理',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_login_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for my_menu
-- ----------------------------
DROP TABLE IF EXISTS `my_menu`;
CREATE TABLE `my_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单表',
  `name` varchar(100) DEFAULT NULL COMMENT '菜单名',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `sort` int(11) DEFAULT '10' COMMENT '排序',
  `is_show` tinyint(1) DEFAULT '1' COMMENT '是否显示 1:显示 2:隐藏',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_menu
-- ----------------------------
BEGIN;
INSERT INTO `my_menu` VALUES (2, '权限管理', '', 16, 1, 1521768073);
INSERT INTO `my_menu` VALUES (3, '日志管理', '', 10, 1, 1521768073);
COMMIT;

-- ----------------------------
-- Table structure for my_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `my_operate_log`;
CREATE TABLE `my_operate_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '操作日志表',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `route_name` varchar(100) DEFAULT NULL COMMENT '路由名',
  `route` varchar(100) DEFAULT NULL COMMENT '路由',
  `param` text COMMENT '参数',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_operate_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for my_role
-- ----------------------------
DROP TABLE IF EXISTS `my_role`;
CREATE TABLE `my_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户角色表',
  `name` varchar(50) DEFAULT NULL COMMENT '角色名',
  `description` varchar(100) DEFAULT NULL COMMENT '角色描述',
  `permission` varchar(500) DEFAULT NULL COMMENT '权限',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_role
-- ----------------------------
BEGIN;
INSERT INTO `my_role` VALUES (1, '超级管理员', '拥有所有权限', '[45,46,47,41,42,43,44,38,39,40,49,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,18,19,20,21,22,12,13,14,15,16,17,1,2,3,4,5,6,48]', 1621921489);
COMMIT;

-- ----------------------------
-- Table structure for my_route
-- ----------------------------
DROP TABLE IF EXISTS `my_route`;
CREATE TABLE `my_route` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '路由表',
  `submenu_id` int(11) DEFAULT NULL COMMENT '子菜单id',
  `route_name` varchar(100) DEFAULT NULL COMMENT '路由名',
  `route` varchar(100) DEFAULT NULL COMMENT '路由',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1:启用 2:禁用',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_route
-- ----------------------------
BEGIN;
INSERT INTO `my_route` VALUES (12, 2, '管理员列表', 'user/index', 1, 1553244064);
INSERT INTO `my_route` VALUES (13, 2, '管理员删除', 'user/del', 1, 1553244091);
INSERT INTO `my_route` VALUES (14, 2, '管理员批量删除', 'user/batch-del', 1, 1553244124);
INSERT INTO `my_route` VALUES (15, 2, '管理员添加', 'user/create', 1, 1553244151);
INSERT INTO `my_route` VALUES (16, 2, '管理员编辑', 'user/update', 1, 1553244172);
INSERT INTO `my_route` VALUES (17, 2, '管理员状态修改', 'user/change-status', 1, 1553244208);
INSERT INTO `my_route` VALUES (18, 3, '角色列表', 'role/index', 1, 1553244281);
INSERT INTO `my_route` VALUES (19, 3, '角色删除', 'role/del', 1, 1553244307);
INSERT INTO `my_route` VALUES (20, 3, '角色批量删除', 'role/batch-del', 1, 1553244335);
INSERT INTO `my_route` VALUES (21, 3, '角色添加', 'role/create', 1, 1553244359);
INSERT INTO `my_route` VALUES (22, 3, '角色编辑', 'role/update', 1, 1553244392);
INSERT INTO `my_route` VALUES (23, 4, '菜单列表', 'menu/index', 1, 1553245834);
INSERT INTO `my_route` VALUES (24, 4, '菜单删除', 'menu/del', 1, 1553245871);
INSERT INTO `my_route` VALUES (25, 4, '菜单批量删除', 'menu/batch-del', 1, 1553246089);
INSERT INTO `my_route` VALUES (26, 4, ' 菜单添加', 'menu/create', 1, 1553246123);
INSERT INTO `my_route` VALUES (27, 4, '菜单编辑', 'menu/update', 1, 1553246169);
INSERT INTO `my_route` VALUES (28, 4, '子菜单列表', 'menu/submenu', 1, 1553246215);
INSERT INTO `my_route` VALUES (29, 4, '子菜单删除', 'menu/submenu-del', 1, 1553246313);
INSERT INTO `my_route` VALUES (30, 4, '子菜单批量删除', 'menu/submenu-batch-del', 1, 1553246362);
INSERT INTO `my_route` VALUES (31, 4, '子菜单添加', 'menu/submenu-create', 1, 1553246414);
INSERT INTO `my_route` VALUES (32, 4, '子菜单更新', 'menu/submenu-update', 1, 1553246441);
INSERT INTO `my_route` VALUES (33, 4, '路由列表', 'menu/route', 1, 1553246467);
INSERT INTO `my_route` VALUES (34, 4, '路由删除', 'menu/route-del', 1, 1553246494);
INSERT INTO `my_route` VALUES (35, 4, '路由批量删除', 'menu/route-batch-del', 1, 1553246527);
INSERT INTO `my_route` VALUES (36, 4, '路由添加', 'menu/route-create', 1, 1553246558);
INSERT INTO `my_route` VALUES (37, 4, '路由编辑', 'menu/route-update', 1, 1553246585);
INSERT INTO `my_route` VALUES (38, 5, '登陆日志列表', 'login-log/index', 1, 1553436275);
INSERT INTO `my_route` VALUES (39, 5, '登陆日志删除', 'login-log/del', 1, 1553436909);
INSERT INTO `my_route` VALUES (40, 5, '登陆日志批量删除', 'login-log/batch-del', 1, 1553436940);
INSERT INTO `my_route` VALUES (41, 6, '操作日志列表', 'operate-log/index', 1, 1553521420);
INSERT INTO `my_route` VALUES (42, 6, '操作日志删除', 'operate-log/del', 1, 1553521445);
INSERT INTO `my_route` VALUES (43, 6, '操作日志批量删除', 'operate-log/batch-del', 1, 1553521474);
INSERT INTO `my_route` VALUES (44, 6, '操作日志查看参数', 'operate-log/view', 1, 1553525306);
INSERT INTO `my_route` VALUES (49, 5, '查看UA', 'login-log/view', 1, 1557309365);
INSERT INTO `my_route` VALUES (50, 7, '失败日志列表', 'login-fail-log/index', 1, 1641969007);
INSERT INTO `my_route` VALUES (51, 7, '失败日志查看', 'login-fail-log/view', 1, 1641969036);
INSERT INTO `my_route` VALUES (52, 7, '失败日志删除', 'login-fail-log/del', 1, 1641969048);
INSERT INTO `my_route` VALUES (53, 7, '失败日志批量删除', 'login-fail-log/batch-del', 1, 1641969082);
COMMIT;

-- ----------------------------
-- Table structure for my_submenu
-- ----------------------------
DROP TABLE IF EXISTS `my_submenu`;
CREATE TABLE `my_submenu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '子菜单表',
  `menu_id` int(11) DEFAULT NULL COMMENT '主菜单id',
  `route_name` varchar(100) DEFAULT NULL COMMENT '路由名',
  `route` varchar(100) DEFAULT NULL COMMENT '路由',
  `sort` int(11) DEFAULT '10' COMMENT '排序',
  `is_show` tinyint(1) DEFAULT '1' COMMENT '是否显示 1:显示 2:隐藏',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_submenu
-- ----------------------------
BEGIN;
INSERT INTO `my_submenu` VALUES (2, 2, '成员管理', 'user/index', 10, 1, 1521768073);
INSERT INTO `my_submenu` VALUES (3, 2, '角色管理', 'role/index', 10, 1, 1521768073);
INSERT INTO `my_submenu` VALUES (4, 2, '菜单管理', 'menu/index', 10, 1, 1521768073);
INSERT INTO `my_submenu` VALUES (5, 3, '登陆日志', 'login-log/index', 10, 1, 1553436248);
INSERT INTO `my_submenu` VALUES (6, 3, '操作日志', 'operate-log/index', 10, 1, 1553521325);
INSERT INTO `my_submenu` VALUES (7, 3, '失败日志', 'login-fail-log/index', 10, 1, 1641968994);
COMMIT;

-- ----------------------------
-- Table structure for my_user
-- ----------------------------
DROP TABLE IF EXISTS `my_user`;
CREATE TABLE `my_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '后台用户表',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `name` varchar(100) DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1:启用 2:禁用',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_user
-- ----------------------------
BEGIN;
INSERT INTO `my_user` VALUES (1, 1, 'admin', '$2y$13$Ln878YXekD23XBX8Xexyse3ys8dFBJRJf3CV5hoB3Uz.CDXgmYIn2', 1, 1521768073);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
